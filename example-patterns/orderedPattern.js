/*
Here's an example configuration for an ordered pattern.
Three audios have to be played at three different locations, ordered from 1 to 3.
*/

import GeoXp from '@mezzo-forte/geoxp';

// step 1: geo configuration
// maps all the relevant locations, provides info for geoFencing
const geo = {
  positions: [
    {
      id: 'pos1',
      label: 'Torino - Piazza Castello',
      lat: 45.0711383,
      lon: 7.685391,
      radius: 60, // meters
      deadband: 40 // meters
    },
    {
      id: 'pos2',
      label: 'Torino - Piazza Vittorio Veneto',
      lat: 45.0651445,
      lon: 7.6929964
      // if no geoFencing parameters are set, this position will use default values
    },
    {
      id: 'pos3',
      label: 'Torino - Gran Madre',
      lat: 45.062657,
      lon: 7.6972366,
      radius: 50, // meters
      fetch: 1.5 // starts loading content 1.5 * radius distance
    }
  ]
};

// step 2: audio configuration
// maps all experience audio content
const audio = {
  sounds: [
    {
      id: "aud1",
      url: "./audio/duck.mp3"
    },
    {
      id: "aud2",
      url: "./audio/people_shouting.mp3"
    },
    {
      id: "aud3",
      url: "./audio/people_talking.mp3"
    }
  ]
};

// step 3: experience pattern configuration
// putting things together
const experience = {
  patterns: [
    // just one pattern is needed
    {
      id: 'exp1',
      label: 'Ordered pattern',
      spots: [
        // first spot is in Piazza Castello (pos1), play duck (aud1)
        {
          id: 'sp1',
          position: 'pos1',
          audio: 'aud1',
          label: "Hello Spot 1"
        },
        // second spot is in Piazza Vittorio (pos2), play shouting (aud2)
        // its content have to be played only if spot 1 has already been visited
        {
          id: 'sp2',
          position: 'pos2',
          audio: 'aud2',
          after: 'sp1', // play content only after "sp1" has been visited
          label: "Hello Spot 2"
        },
        // third spot is at Gran Madre (pos3), play talking (aud3)
        // its content have to be played only if spot 2 has already been visited
        {
          id: 'sp3',
          position: 'pos3',
          audio: 'aud3',
          after: 'sp2', // play content only after "sp2" has been visited
          label: "Hello Spot 3"
        },
      ]
    }
  ]
};

// compose configuration object and create geoXp instance on configuration
const geoXp = new GeoXp({ geo, audio, experience });